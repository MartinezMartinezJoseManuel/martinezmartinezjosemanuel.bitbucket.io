var Ajedrez = (function() {
	var _mostrar_tablero = function() 
	{
		//Instanciamiento de los elemntos del index
		var tab = document.getElementById("tablero");
		var men = document.getElementById("mensaje");
		var opc = document.getElementById("opcion");

		//agregamos los elementos al index
		//---------SE AGREGA EL BOTON---------
		var boton = document.createElement("input");
		boton.setAttribute("type","button");
		boton.setAttribute("value","Actualizar Tablero");
		boton.addEventListener("click",_actualizar_tablero,false);
		opc.appendChild(boton);
		
		var tabla = document.createElement("table");
		var tblBody = document.createElement("tbody");
		
		tabla.appendChild(tblBody);
		tab.appendChild(tabla);


		//--------SE AGREGA EL TABLERO--------
		var url = "https://martinezmartinezjosemanuel.bitbucket.io/csv/tablero.csv";
		var xhr = new XMLHttpRequest();
		
		xhr.open('GET', url, true);
		xhr.onreadystatechange = function ()
		{
			if(xhr.readyState === 4)
			{
				if(xhr.status === 200)
				{
					var tab = document.getElementById("tablero")
					
					var tabla = document.createElement("table");
					var tblBody = document.createElement("tbody");
					

					//Obtenemos el archivo csv
					var data = xhr.responseText;
					//separamos cada columna mediante una expresion regular que  busca el salto de linea
					var allRows = data.split(/\r?\n|\r/);
					for (var i = 0; i < 9; i++) 
					{
						//cada celda esta dividida por el caracter '|'
						var rowCells = allRows[i].split('|');
						//para las filas se crea un elemento tr
						var fila = document.createElement("tr");
						//se recorre en busca de todas las filas
						for(var j = 0;j<9;j++)
						{
							//si el texto de las celdas es '∅' se sustituye por un espacio en blanco
							//esto es simplemente por estetica
							var textoCelda = ' ';
							if(rowCells[j] !== '∅')
								textoCelda = rowCells[j];
							
							//se define la fila 0 y la columna 0 como elementos tipo cabecera
							//la fila 0 y la columna 0 solamente tienen los indices de la tabla con los que se trabajaran
							if(i === 0 || j===0)
							{
								var cabecera = document.createElement("th");
								cabecera.textContent = textoCelda;
								cabecera.setAttribute("class",cabecera)
								fila.appendChild(cabecera);
							}
							else
							{
								//si no son cabeceras se agregan los elementos del tablero
								//cada elemento con la pieza que tenga y se le asigna una clase
								//la clase puede ser blanca o negra, esto es por pura estetica.
								var celda = document.createElement("td");
								if(i%2 == 0)
									if(j%2==0)
										celda.setAttribute("class", "blancas");
									else
										celda.setAttribute("class", "negras");
								else
									if(j%2==0)
										celda.setAttribute("class", "negras");
									else
										celda.setAttribute("class", "blancas");
								//a las celdas les damos el texto que le corresponde ya sea pieza o espacio enblanco
								celda.textContent= textoCelda;
								//agregamos la celda a la fila
								fila.appendChild(celda);
							}
						}
						//agregamos la fila al cuerpo de la tabla
						tblBody.appendChild(fila);
					}
					//le damos un id a la tabla y le agregamos su cuerpo
					tabla.setAttribute("id","tabTablero");
					tabla.appendChild(tblBody);
					//la tabla la agregamos al contenedor
					tab.appendChild(tabla);
				}
				else
				{
					//si hubo un error al abrir el archivo lo mandamos aqui.
					var men = document.getElementById("mensaje");
					var mensaje = document.createElement("p");

					mensaje.setAttribute("class","advertencia");
					mensaje.textContent = "Error " + this.status + " " + this.statusText + " - " + this.responseURL;
					men.appendChild(mensaje);
				}
			}
		}
		xhr.send(null);
	};
	
	var _actualizar_tablero = function() 
	{
		//Esta funcion te permite recargar el archivo csv y descargar el estado actual del csv
		var csv = [];
    	var rows = document.querySelectorAll("table tr");
    
		for (var i = 0; i < rows.length; i++) 
		{
	        var row = [], cols = rows[i].querySelectorAll("td, th");
        
			for (var j = 0; j < cols.length; j++) 
			{
				if(cols[j].innerText === '')
					row.push('∅');
				else
					row.push(cols[j].innerText);
			}
        	csv.push(row.join("|"));        
    	}

    	// Download CSV file
		downloadCSV(csv.join("\n"), "tablero.csv");
		
		_mostrar_tablero;
	};

	function downloadCSV(csv, filename) 
	{
		var csvFile;
		var downloadLink;
		csvFile = new Blob([csv], {type: "text/csv"});
		downloadLink = document.createElement("a");
		downloadLink.download = filename;
		downloadLink.href = window.URL.createObjectURL(csvFile);
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
		downloadLink.click();
	}

	var _mover_pieza = function(obj) {
		//Limpiamos los mensajes
		var advertencia = document.getElementsByTagName('p');
		mensaje.textContent="";

		//obtenemos las coordenadas de cada elemento
		var origen = obj.de;
		var destino = obj.a;
		if(origen === undefined || destino === undefined)
		{
			advertencia = document.getElementsByTagName('p');
			mensaje.textContent="Se debe contar con un de:coordenada y un a:coordenada";
		}
		var coLet = {"a":1,"b":2,"c":3,"d":4,"e":5,"f":6,"g":7,"h":8}
		
		var coAux = origen.substring(0,1);
		var xo = coLet[coAux];
		var yo = origen.substring(1,2);
		coAux = destino.substring(0,1);
		var xd = coLet[coAux];
		var yd = destino.substring(1,2);

		if(xo<=8 && xo >= 1 && yo<=8 && yo >= 1 && yd<=8 && yd >= 1 && xd<=8 && xd >= 1)
		{
			var filas = document.getElementsByTagName('tr');
			var celdaor = filas[9-yo].children[xo].innerText;
			var celdade = filas[9-yd].children[xd].innerText;
			
			if(celdade ==='')
			{
				filas[9-yd].children[xd].textContent = celdaor;
				filas[9-yo].children[xo].textContent = ' ';
			}
			else
			{
				advertencia = document.getElementsByTagName('p');
				mensaje.textContent="la posicion ya esta ocupada";
			}
		}
		else
		{
			advertencia = document.getElementsByTagName('p');
			mensaje.textContent="El rango de las coordenazas es [a-z][1-8]";
		}

	};
	return {
		"mostrarTablero": _mostrar_tablero,
		"actualizarTablero": _actualizar_tablero,
		"moverPieza": _mover_pieza
	}
})();
